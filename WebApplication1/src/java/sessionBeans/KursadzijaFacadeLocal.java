/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionBeans;

import entiteti.Kursadzija;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Grupa1
 */
@Local
public interface KursadzijaFacadeLocal {

    void create(Kursadzija kursadzija);

    void edit(Kursadzija kursadzija);

    void remove(Kursadzija kursadzija);

    Kursadzija find(Object id);

    List<Kursadzija> findAll();

    List<Kursadzija> findRange(int[] range);

    int count();
    
}
