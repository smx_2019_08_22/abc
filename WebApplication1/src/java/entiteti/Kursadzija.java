/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entiteti;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Grupa1
 */
@Entity
@Table(name = "kursadzija")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Kursadzija.findAll", query = "SELECT k FROM Kursadzija k")
    , @NamedQuery(name = "Kursadzija.findById", query = "SELECT k FROM Kursadzija k WHERE k.id = :id")
    , @NamedQuery(name = "Kursadzija.findByIme", query = "SELECT k FROM Kursadzija k WHERE k.ime = :ime")
    , @NamedQuery(name = "Kursadzija.findByPrezime", query = "SELECT k FROM Kursadzija k WHERE k.prezime = :prezime")})
public class Kursadzija implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 15)
    @Column(name = "ime")
    private String ime;
    @Size(max = 25)
    @Column(name = "prezime")
    private String prezime;

    public Kursadzija() {
    }

    public Kursadzija(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Kursadzija)) {
            return false;
        }
        Kursadzija other = (Kursadzija) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entiteti.Kursadzija[ id=" + id + " ]";
    }
    
}
